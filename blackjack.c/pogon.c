﻿#include "zaglavlje.h"

int vrijednostRuke(int ruka[], int brojKarata) {
	int bodovi = 0;
	int brojAsova = 0;
	for (int i = 0; i < brojKarata; i++) {
		if (ruka[i] == 11) {
			brojAsova++;
		}
		bodovi += ruka[i];
	}
	
	while (bodovi > 21 && brojAsova > 0) {
		bodovi -= 10; // As se računa kao 1 umjesto 11
		brojAsova--;
	}
	return bodovi;
}



void ispisiRuku(int ruka[], int brojKarata) {
	srand(time(NULL));
	
	for (int i = 0; i < brojKarata; i++) {
		printf("%d ", ruka[i]);
	}
	printf("\n");
}


void pravila(){
	printf("\t\t\tPRAVILA BLACKJACKA:\n");
	printf("\n\tBlackjack se igra protiv kuce. Cilj igre je uzeti karte sto blize, \n\t     ali ne i vise od vrijednost 21 i pobijediti djelitelja.");
	printf("\n\n\t\t   Medutim, ako Vasa ruka prijede 21, \n\tsmatrati ce se da ste izgubili bez obzira na ruku djelitelja.");
	printf("\n\n\t\tKralj, kraljica i decko vrijede 10 bodova.");
	printf("\n\t    Karte s brojevima zadrzavaju nominalnu vrijednost.");
	printf("\n  Asevi(11) se mogu racunati kao 1 bod ili 11 bodova, ovisno o tome kako vise odgovara igracu.");
	printf("\n\n\t\t    SVI NOVI IGRACI KRECU S 500 DOLARA.");
	
	printf("\n\n\n"); 
}

int start() {
    srand(time(NULL));
    float balance = 500.0;
    int nastaviti = 1;

    while (nastaviti) {
        int spil[52] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10,
                         2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10,
                         2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10,
                         2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10,
                         11, 11, 11, 11 }; 


        HAND* rukaDiler = izradiHand();
        HAND* rukaIgrac = izradiHand();
        int bodoviDiler, bodoviIgrac;
        char izb;
        float ulog = 0;

        printf("Diler: Dobrodosli za stol! :)\n\n");
        printf("Vas novcanik: %.2f $\n", balance);
        printf("Dodajte ulog: ");
        scanf("%f", &ulog);

        if (ulog > balance) {
            printf("Nemate dovoljno sredstava na racun.\n");
            freeHand(rukaDiler);
            freeHand(rukaIgrac);
            return 0;
        }

        // Pocetna ruka dilera i igraca
        dodajKartu(rukaDiler, spil[rand() % 52]);
        dodajKartu(rukaDiler, spil[rand() % 52]);
        dodajKartu(rukaIgrac, spil[rand() % 52]);
        dodajKartu(rukaIgrac, spil[rand() % 52]);

        printf("\nTvoja ruka:\n");
        ispisiRuku(rukaIgrac->karte, rukaIgrac->velicina);
        bodoviIgrac = vrijednostRuke(rukaIgrac->karte, rukaIgrac->velicina);
        printf("Vrijednost tvoje ruke: %d\n\n", bodoviIgrac);

        printf("Ruka dilera:\n");
        ispisiRuku(rukaDiler->karte, rukaDiler->velicina);
        bodoviDiler = vrijednostRuke(rukaDiler->karte, rukaDiler->velicina);
        printf("Vrijednost dilerove ruke: %d\n\n", bodoviDiler);

        if (bodoviIgrac == 21) {
            balance = balance + (ulog * 1.5);
            printf("\n\nImate Blackjack!!! Pobijedili ste! (+%.2f$)\n\n", ulog * 1.5);
            freeHand(rukaDiler);
            freeHand(rukaIgrac);
            return 0;
        }

        if (bodoviDiler == 21) {
            balance = balance - ulog;
            printf("\n\nDiler ima blackjack. Vise srece drugi put!:((-% .2f$)\n\n", ulog);
            freeHand(rukaDiler);
            freeHand(rukaIgrac);
            return 0;
        }

        while (1) {
            printf("Zelite li vuci novu kartu? (D/N): ");
            scanf(" %c", &izb);

            if (izb == 'n' || izb == 'N') {
                break;
            }
            if (izb == 'D' || izb == 'd') {
                dodajKartu(rukaIgrac, spil[rand() % 52]);
                printf("\n\nTvoje nove karte su:\n");
                ispisiRuku(rukaIgrac->karte, rukaIgrac->velicina);
                bodoviIgrac = vrijednostRuke(rukaIgrac->karte, rukaIgrac->velicina);
                printf("Vrijednost tvoje ruke: %d\n", bodoviIgrac);
                printf("Vrijednost dilerove ruke: %d\n\n", bodoviDiler);

                if (bodoviIgrac > 21) {
                    balance = balance - ulog;
                    printf("\nPremasili ste 21! Izgubili ste. :( (-%.2f$)\n\n", ulog);
                    freeHand(rukaDiler);
                    freeHand(rukaIgrac);
                    return 0;
                }
            }
            else {
                printf("Nepoznat unos. (D/N)\n");
            }
        }

        while (bodoviDiler < 17) {
            dodajKartu(rukaDiler, spil[rand() % 52]);
            bodoviDiler = vrijednostRuke(rukaDiler->karte, rukaDiler->velicina);
            printf("Diler uzima kartu. Novi bodovi dilerove ruke: %d\n", bodoviDiler);
        }

        printf("Karte dilera su:\n");
        ispisiRuku(rukaDiler->karte, rukaDiler->velicina);
        printf("Vrijednost dilerove ruke: %d\n", bodoviDiler);

        if (bodoviDiler == bodoviIgrac) {
            printf("\n\nNerijeseno. (+0$)\n");
        }
        else if (bodoviDiler > 21 || (bodoviIgrac <= 21 && bodoviIgrac > bodoviDiler)) {
            balance = balance + ulog;
            printf("\n\nPobijedili ste! (+%.2f$)\n\n", ulog);
        }
        else {
            balance = balance - ulog;
            printf("\n\nDiler pobjeduje. Vise srece drugi put!:((-% .2f$)\n\n", ulog);
        }

        freeHand(rukaDiler);
        freeHand(rukaIgrac);

        printf("Zelite li igrati ponovo? (1 za da / 0 za ne): ");
        scanf("%d", &nastaviti);
    }

    return;
}


int izlaz() {
	
	char izl = '\0';
	
	do {
		printf("Jeste li sigurni?  (D/N)");
		scanf(" %c", &izl);

		if (izl == 'D' || izl == 'd') {
			printf("\nVidimo se uskoro!\n");
			return 0;
		}
		else if (izl == 'n' || izl == 'N') {
			printf("\n");
			return 1;
		}
		else {
			printf("Nepoznat unos. (D/N)\n");
		}
	} while (1);

	return 0;
}

HAND* izradiHand() {
	HAND* hand = (HAND*)malloc(sizeof(HAND));
	hand->karte = (int*)malloc(10 * sizeof(int)); 
	hand->velicina = 0;
	hand->kapacitet = 10;
	return hand;
}

void dodajKartu(HAND* hand, int card) {
	if (hand->velicina >= hand->kapacitet) {
		hand->kapacitet *= 2;
		hand->karte = (int*)realloc(hand->karte, hand->kapacitet * sizeof(int));
	}
	hand->karte[hand->velicina++] = card;
}

void freeHand(HAND* hand) {
	free(hand->karte);
	free(hand);
}
