#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct {
    int* karte;
    int velicina;
    int kapacitet;
} HAND;


int izlaz(); // izlaz iz igre
int start(); // pokrece igru
void pravila(); //ispisuje pravila
int vrijednostRuke(int ruka[], int brojKarata); //izracunava vrijednost trenutne ruke
void ispisiRuku(int ruka[], int brojKarata); // ispisuje vrijednost ruke
HAND* izradiHand();
void dodajKartu(HAND* hand, int card);
void freeHand(HAND* hand);





